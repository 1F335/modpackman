# -*- mode: python ; coding: utf-8 -*-
import os
import sys

# included data is based on cwd
cwd = os.getcwd()
pack_name = cwd.split(os.path.sep)[-1]
j = os.path.join

block_cipher = None


a = Analysis(['installer.py'],
             pathex=[cwd],
             binaries=[],
             datas=[
                 (j(cwd, 'pack-lock.ini'), '.'),
                 (j(cwd, 'pack.ini'), '.'), 
                 (j(cwd, 'icon.png'), '.'), 
                 (j(cwd, 'config'), 'config')
             ],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)

pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name=f'{pack_name}-installer',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=True,
	  icon=j(cwd, 'icon.ico') + ",0")
